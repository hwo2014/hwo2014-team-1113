set plr_count=1
set password=reso
set server=hakkinen.helloworldopen.com

mkdir run_all_keimola
mkdir run_all_germany
mkdir run_all_france
mkdir run_all_usa
mkdir run_all_elaeintarha
mkdir run_all_suzuka
mkdir run_all_imola
mkdir run_all_england

cd run_all_keimola
start ..\bin\debug\Resocar.exe %server% 8091 Resocar E/fT/A4ziAoxVg keimola 1 %password%
cd..
@ping 127.0.0.1 -n 2 -w 1000 > NUL
cd run_all_germany
start ..\bin\debug\Resocar.exe %server% 8091 Resocar E/fT/A4ziAoxVg germany 1 %password%
cd..
@ping 127.0.0.1 -n 2 -w 1000 > NUL
cd run_all_france
start ..\bin\debug\Resocar.exe %server% 8091 Resocar E/fT/A4ziAoxVg france 1 %password%
cd..
@ping 127.0.0.1 -n 2 -w 1000 > NUL
cd run_all_usa
start ..\bin\debug\Resocar.exe %server% 8091 Resocar E/fT/A4ziAoxVg usa 1 %password%
cd..
@ping 127.0.0.1 -n 2 -w 1000 > NUL
cd run_all_elaeintarha
start ..\bin\debug\Resocar.exe %server% 8091 Resocar E/fT/A4ziAoxVg elaeintarha 1 %password%
cd..
@ping 127.0.0.1 -n 2 -w 1000 > NUL
cd run_all_suzuka
start ..\bin\debug\Resocar.exe %server% 8091 Resocar E/fT/A4ziAoxVg suzuka 1 %password%
cd..
@ping 127.0.0.1 -n 2 -w 1000 > NUL
cd run_all_imola
start ..\bin\debug\Resocar.exe %server% 8091 Resocar E/fT/A4ziAoxVg imola 1 %password%
cd..
@ping 127.0.0.1 -n 2 -w 1000 > NUL
cd run_all_england
start ..\bin\debug\Resocar.exe %server% 8091 Resocar E/fT/A4ziAoxVg england 1 %password%
cd..


pause