﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Diagnostics;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

enum Switch
{
    Undecided,
    NoSwitch,
    Left,
    Right
}

public class Bot
{
    public static void Main(string[] args)
    {
        CacheJson();

        string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];
		
        string track = args.Length > 4 ? args[4] : null;
        int playerCount = args.Length > 5 ? int.Parse(args[5]) : 1;
        string password = args.Length > 6 ? args[6] : null;
		
        Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        using (TcpClient client = new TcpClient(host, port))
        {
			client.NoDelay = true;
            NetworkStream stream = client.GetStream();
            StreamReader reader = new StreamReader(stream);
            StreamWriter writer = new StreamWriter(stream);
            writer.AutoFlush = true;

            if (track == null)
            {
                new Bot(reader, writer, new Join(botName, botKey));
            }
            else
            {
                new Bot(reader, writer, new JoinRace(botName, botKey, track, password, playerCount));
            }  
        }
    }

    private void PrintLine(Object obj)
    {
        String str = obj.ToString();
        Console.WriteLine(str);
        if (debugOut != null)
        {
            debugOut.WriteLine(str);
        }
    }

    private void Print(Object obj)
    {
        String str = obj.ToString();
        Console.Write(str);
        if (debugOut != null)
        {
            debugOut.Write(str);
        }
    }

    static private void CacheJson()
    {
        string line = "{'asdf': 123}";
        string carPos = "{\"msgType\":\"carPositions\",\"data\":[{\"id\":{\"name\":\"Resocar\",\"color\":\"red\"},\"angle\":0.0,\"piecePosition\":{\"pieceIndex\":0,\"inPieceDistance\":0.0,\"lane\":{\"startLaneIndex\":0,\"endLaneIndex\":0},\"lap\":0}}],\"gameId\":\"f03fc9c7-32a9-4f30-a493-af4de496ef0e\"}";
        string gameInit = "{\"msgType\":\"gameInit\",\"data\":{\"race\":{\"track\":{\"id\":\"pentag\",\"name\":\"Penta G\",\"pieces\":[{\"length\":100.0},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"length\":100.0},{\"length\":30.0},{\"radius\":200,\"angle\":22.5},{\"radius\":150,\"angle\":45.0},{\"radius\":100,\"angle\":75.0},{\"radius\":50,\"angle\":80.0,\"switch\":true},{\"radius\":50,\"angle\":45.0},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-42.5},{\"length\":70.0,\"switch\":true},{\"length\":70.0},{\"radius\":75,\"angle\":-90.0},{\"radius\":90,\"angle\":-35.0},{\"length\":100.0},{\"length\":110.0,\"switch\":true},{\"length\":50.0},{\"radius\":100,\"angle\":75.0},{\"radius\":100,\"angle\":45.0,\"switch\":true},{\"radius\":150,\"angle\":25.0},{\"radius\":50,\"angle\":85.0},{\"length\":50.0},{\"radius\":50,\"angle\":-90.0},{\"length\":100.0},{\"radius\":50,\"angle\":75.5},{\"length\":20.0},{\"length\":80.0},{\"radius\":80,\"angle\":-45.0,\"switch\":true},{\"radius\":80,\"angle\":-45.0},{\"radius\":80,\"angle\":-45.0},{\"radius\":80,\"angle\":-45.0},{\"radius\":80,\"angle\":-45.0},{\"radius\":80,\"angle\":-45.0},{\"length\":107.5},{\"radius\":55,\"angle\":-90.0,\"switch\":true},{\"length\":127.5},{\"radius\":50,\"angle\":-90.0},{\"length\":90.0},{\"length\":70.0,\"switch\":true},{\"length\":20.0},{\"radius\":50,\"angle\":-30.0},{\"radius\":75,\"angle\":-30.0},{\"radius\":60,\"angle\":30.0},{\"radius\":75,\"angle\":30.0},{\"radius\":75,\"angle\":30.0},{\"radius\":75,\"angle\":30.0},{\"radius\":67,\"angle\":30.0},{\"radius\":65,\"angle\":30.0},{\"radius\":70,\"angle\":60.0,\"switch\":true},{\"length\":260.0}],\"lanes\":[{\"distanceFromCenter\":-20,\"index\":0},{\"distanceFromCenter\":0,\"index\":1},{\"distanceFromCenter\":20,\"index\":2}],\"startingPoint\":{\"position\":{\"x\":-340.0,\"y\":-96.0},\"angle\":90.0}},\"cars\":[{\"id\":{\"name\":\"ResocarPekuja\",\"color\":\"red\"},\"dimensions\":{\"length\":40.0,\"width\":20.0,\"guideFlagPosition\":10.0}}],\"raceSession\":{\"laps\":10,\"maxLapTimeMs\":60000,\"quickRace\":true}}},\"gameId\":\"3cf29261-6d3f-4d2d-a307-2947cf684ae6\"}";
        string joinRace = "{\"msgType\":\"joinRace\",\"data\":{\"botId\":{\"name\":\"ResocarPekuja\",\"key\":\"E/fT/A4ziAoxVg\"},\"trackName\":\"pentag\",\"password\":null,\"carCount\":1},\"gameTick\":0}";
        string yourCar = "{\"msgType\":\"yourCar\",\"data\":{\"name\":\"ResocarPekuja\",\"color\":\"red\"},\"gameId\":\"0e90e79e-c8ea-473b-a8ca-8db091c0362e\"}";
        string gameStart = "{\"msgType\":\"gameStart\",\"data\":null,\"gameId\":\"0e90e79e-c8ea-473b-a8ca-8db091c0362e\",\"gameTick\":0}";

        JsonConvert.DeserializeObject<MsgWrapper>(line);
        JsonConvert.DeserializeObject<ReceiveMsg>(gameStart);
        JsonConvert.DeserializeObject<Join>(line);
        JsonConvert.DeserializeObject<BotId>(line);
        JsonConvert.DeserializeObject<JoinRace>(joinRace);
        JsonConvert.DeserializeObject<Throttle>(line);
        JsonConvert.DeserializeObject<SwitchLane>(line);
        JsonConvert.DeserializeObject<LanePosition>(line);
        JsonConvert.DeserializeObject<PiecePosition>(line);
        JsonConvert.DeserializeObject<CarPosition>(line);
        JsonConvert.DeserializeObject<CarId>(line);
        JsonConvert.DeserializeObject<CarPositions>(carPos);
        JsonConvert.DeserializeObject<Piece>(line);
        JsonConvert.DeserializeObject<Lane>(line);
        JsonConvert.DeserializeObject<Track>(line);
        JsonConvert.DeserializeObject<RaceSession>(line);
        JsonConvert.DeserializeObject<RaceInfo>(line);
        JsonConvert.DeserializeObject<Race>(line);
        JsonConvert.DeserializeObject<GameInit>(gameInit);
        JsonConvert.DeserializeObject<Crash>(line);
        JsonConvert.DeserializeObject<Spawn>(line);
        JsonConvert.DeserializeObject<TurboStart>(line);
        JsonConvert.DeserializeObject<TurboEnd>(line);
        JsonConvert.DeserializeObject<YourCar>(yourCar);
        JsonConvert.DeserializeObject<Turbo>(line);
        JsonConvert.DeserializeObject<TurboAvailable>(line);
        JsonConvert.DeserializeObject<TurboData>(line);
        JsonConvert.DeserializeObject<RaceLog>(line);
        JsonConvert.DeserializeObject<RaceLogEntry>(line);

        JsonConvert.SerializeObject(new MsgWrapper("asdf", null, 123));
        JsonConvert.SerializeObject(new Join("name", "key"));
        JsonConvert.SerializeObject(new BotId("name", "key"));
        JsonConvert.SerializeObject(new JoinRace("name", "key", "track", "password", 7));
        JsonConvert.SerializeObject(new Throttle(1.0, 123));
        JsonConvert.SerializeObject(new SwitchLane("Left", 412));
        JsonConvert.SerializeObject(new CarId());
        JsonConvert.SerializeObject(new Turbo(412));
        /*        
                TurboAvailable turbo = JsonConvert.DeserializeObject<TurboAvailable>(line);
                TurboStart turboStart = JsonConvert.DeserializeObject<TurboStart>(line);
                GameInit init = JsonConvert.DeserializeObject<GameInit>(line);
                TurboEnd turboEnd = JsonConvert.DeserializeObject<TurboEnd>(line);
                MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
                YourCar yourCar = JsonConvert.DeserializeObject<YourCar>(line);
                CarPositions carPositions = JsonConvert.DeserializeObject<CarPositions>(line);
                Spawn spawn = JsonConvert.DeserializeObject<Spawn>(line);
                Crash crash = JsonConvert.DeserializeObject<Crash>(line);
*/
    }

    private StreamWriter debugOut;
    private StreamWriter writer;
    private string m_lastSentCommand = null;

    private bool sendTicks;

    Bot(StreamReader reader, StreamWriter writer, SendMsg join)
    {
        sendTicks = true;

        this.writer = writer;
        string line;

        send(join);

        List<RaceLogEntry> raceLog = new List<RaceLogEntry>();

//        StreamWriter serverInput = new StreamWriter("serverInput.json");
//        StreamWriter botOutput = new StreamWriter("botOutput.json");
        debugOut = new StreamWriter("debugLog.txt");
//        debugOut = null;

        int numLaps = 0;

        double lastPosition = 0.0;
        double lastVelocity = 0.0;
        double lastAngle = 0.0;
        double lastLastAngle = 0.0;
        double largestAngle = 0.0;
        double lastThrottle = 0.0;
        double lastLastThrottle = 0.0;
        int lastStartLane = -1;
        int lastEndLane = -1;
        
        Piece lastPiece = null;
  
        bool hasTurbo = false;
        TurboData turboData = null;
        double currentTurboFactor = 1.0;
        double lastTurboFactor = 1.0;
        int turboTicksLeft = 0;
        bool fullOnTurbo = false;

        double friction = 0.98;
        double enginePower = 0.2;
        bool frictionCalculated = false;
        bool enginePowerCalculated = false;

        //double safeSpeed = 4.0;//Math.Sqrt (0.32 * 90); //sqrt(28.8), 28.8 = 0.32 * 90 (90 = curve radius)

        string myCarColor = "";

        Piece[] pieces = new Piece[0];
        Lane[] lanes = new Lane[0];
        int[] laneSwitches = new int[0];
        int pieceId = 0;
        int switchLanes = 0;
        Switch nextSwitch = Switch.Undecided;
		
        Stopwatch stopwatch = new Stopwatch();
        List<string> crashedEnemies = new List<string>();
        CarPositions lastCarPositions = null;
        List<string> carsToAvoid = new List<string>();
        List<string> carsToAvoidBehind = new List<string>();
        double[] pieceAngleLimits = null;
        double closestEnemyVelocity = -1.0;
		
        List<double> calculatedSafeSpeedFactors = new List<double>();
        List<double> calculatedSpeedCoefficients = new List<double>();
        List<double> calculatedAngleCoefficients = new List<double>();
        List<double> calculatedDAngleCoefficients = new List<double>();

        List<double> ddAngles = new List<double>();
        List<double> radii = new List<double>();
        List<double> velocities = new List<double>();
        List<double> angles = new List<double>();
        List<double> dAngles = new List<double>();

        double safeSpeedFactor = 0.2;
        double speedCoefficient = 0.3;
        double angleCoefficient = -0.00125;
        double dAngleCoefficient = -0.1;
		
        int startingRank = -1;
		
        int longestStraightPieceIndex = -1;

		int crashCount = 0;
        
        bool isCrashed = false;
        long milliseconds = 0;

        double angleLimitMod = 0.0;
        double targetLimitMod = 0.0;

        double carLength = 0.0;

        double hardAngleLimit = 60.0;

        bool bumped = false;
        bool lastBumped = false;

        while ((line = reader.ReadLine()) != null)
        {
            stopwatch.Reset();
            stopwatch.Start();
//            serverInput.WriteLine(line);
            MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
            switch (msg.msgType)
            {
            case "gameInit":
                GameInit init = JsonConvert.DeserializeObject<GameInit>(line);

                numLaps = init.data.race.raceSession.laps;
                pieces = init.data.race.track.pieces;
                lanes = init.data.race.track.lanes;
                laneSwitches = new int[pieces.Length];
                int currentSwitchPiece = -1;

                double laneValue = 0.0;
                int pieceCount = pieces.Length;
                for (int i = 0; i < pieceCount; ++i)
                {
                    Piece piece = pieces[(i + 1) % pieces.Length];

                    if (piece.radius > 0)
                    {
                        laneValue += piece.angle / piece.radius;
                    }

                    if (piece.@switch)
                    {
                        if (currentSwitchPiece >= 0)
                        {
                            laneSwitches[currentSwitchPiece] = (laneValue > 0.0 ? 1 : (laneValue < 0.0 ? -1 : 0));                            
                        }
                        else
                        {
                            pieceCount += i + 1;
                        }
                        laneValue = 0.0;
                        currentSwitchPiece = i;
                    }
                    else
                    {
                        laneSwitches[i % pieces.Length] = 0;
                    }
                }
                
                // Set empty lane switch pieces to the switch we're taking next
                for (int i = 0; i < pieces.Length; ++i)
                {
                    Piece piece = pieces[(i + 1) % pieces.Length];
                    
                    if (piece.@switch && laneSwitches[i % pieces.Length] == 0)
                    {
                        for (int j = i + 1; j < pieceCount; j++)
                        {
                            if (laneSwitches[j % pieces.Length] != 0)
                            {
                                laneSwitches[i % pieces.Length] = laneSwitches[j % pieces.Length];
                                break;
                            }
                        }
                    }
                }
                
                
                if (pieceAngleLimits == null)
                    pieceAngleLimits = new double[pieceCount];
                
                // Add angle limits
                for (int i = 0; i < pieces.Length; i++)
                {
                    Piece piece = pieces[i];
                    //PrintLine (i + " " + piece.ToString());

                    if (piece.@switch && piece.radius > 0)
                        pieceAngleLimits[i] = 59;
                    else if (piece.@switch)
                        pieceAngleLimits[i] = 59.5;
                    else
                        pieceAngleLimits[i] = 59.999;
                }
				
				// Calculate the longest straight
                double longestStraightDistance = 0.0;
                double currentStraightDistance = 0.0;
                int currentStraightStartPiece = -1;
                int turnPiecesIgnored = 0;
                for (int i = 0; i < pieces.Length * 2; i++)
                {
                    Piece piece = pieces[i % pieces.Length];
                    if (piece.radius > 0 && (piece.radius < 200 || currentStraightStartPiece == -1 || turnPiecesIgnored > 1))
                    {
                        //PrintLine("Piece " + i + " is " + piece.ToString());
                        //if (currentStraightDistance > 0)
                        //PrintLine("Straight distance " + currentStraightDistance + " from piece " + currentStraightStartPiece + " to " + ((i - 1) % pieces.Length));
								
                        if (currentStraightDistance > longestStraightDistance)
                        {
                            longestStraightPieceIndex = currentStraightStartPiece;
                            longestStraightDistance = currentStraightDistance;
                        }
						
                        currentStraightStartPiece = -1;
                        currentStraightDistance = 0.0;
                        turnPiecesIgnored = 0;
                    }
                    else
                    {
                        if (piece.radius > 0)
                            turnPiecesIgnored++;

                        if (currentStraightStartPiece == -1)
                        {
                            currentStraightStartPiece = i % pieces.Length;
                        }

                        // Small penalty for turn pieces
                        if (piece.radius > 0)
                            currentStraightDistance -= 40;
                        else
                            currentStraightDistance += piece.length;
                    }
                }
                PrintLine("Longest straight start piece is " + longestStraightPieceIndex + ", distance: " + longestStraightDistance);

                foreach (CarInfo carInfo in init.data.race.cars)
                {
                    if (carLength == 0.0)
                    {
                        carLength = carInfo.dimensions.length;
                        PrintLine("Car length: " + carLength);
                    }
                    else if (carLength != carInfo.dimensions.length)
                    {
                        PrintLine("DIFFERENT CAR LENGHTS DETECTED: " + carLength + ", " + carInfo.dimensions.length);
                    }
                }

                PrintLine("Race init");
                PrintLine(line);
//                send(new Ping());
                break;
            case "yourCar":
                PrintLine(line);
                YourCar yourCar = JsonConvert.DeserializeObject<YourCar>(line);

                myCarColor = yourCar.data.color;
                break;
            case "carPositions":
                CarPositions carPositions = JsonConvert.DeserializeObject<CarPositions>(line);

                CarPosition myCar = null;

                foreach (CarPosition carPos in carPositions.data)
                {
                    if (carPos.id.color == myCarColor)
                    {
                        myCar = carPos;
                    }
                }

                //double targetLimitMod = -8.0 * carsToAvoidBehind.Count;

                pieceId = myCar.piecePosition.pieceIndex;

                if (carPositions.gameTick > 0 && myCar.prevCommandTick != carPositions.gameTick - 1)
                {
                    PrintLine("MISSED " + (carPositions.gameTick - 1 - myCar.prevCommandTick) + " TICK(S)!");
                    lastThrottle = lastLastThrottle;
                    sendTicks = false;
                }
                else
                {
                    sendTicks = true;
                }

                double currentAngle = myCar.angle;
                double angleDiff = currentAngle - lastAngle;

                int currentStartLane = myCar.piecePosition.lane.startLaneIndex;
                int currentEndLane = myCar.piecePosition.lane.endLaneIndex;
                double startLaneOffset = lanes[currentStartLane].distanceFromCenter;
                double endLaneOffset = lanes[currentEndLane].distanceFromCenter;

                double position = myCar.piecePosition.inPieceDistance;
                double velocity = position - lastPosition;
                bool turboUsed = false;
                double throttle = 1.0;

                int myRank = 1;

                if (carPositions.gameTick >= 0)
                {
                    Piece currentPiece = pieces[pieceId];

                    if (lastPiece != null && lastPiece != pieces[pieceId])
                    {
                        if (switchLanes != 0)
                        {
                            PrintLine("MISSED CHANCE TO SWITCH LANES");
                            switchLanes = 0;
                        }
                        nextSwitch = Switch.Undecided;
                        //                    PrintLine ("Hopped over a piece edge");
                        if (lastStartLane != currentStartLane)
                        {
                            velocity = (lastVelocity * friction + lastThrottle * enginePower * lastTurboFactor);

                            if (velocity < position)
                            {
                                PrintLine("Somebody probably bumped into us, because our velocity seems wrong");
                                PrintLine("Position: " + position + " velocity: " + velocity);
                                // This didn't really work, because there's a displacement, and our velocity
                                // isn't actually that high.
                                //                            velocity = position;
                            }
                        }
                        else
                        {
                            velocity = position + (lastPiece.getLength(lanes[lastStartLane].distanceFromCenter, startLaneOffset) - lastPosition);
                        }


                        //                    if (lastStartLane != currentStartLane)
                        //                    {
                        //                        double positionVelocity = position + (lastPiece.getLength(lanes[lastStartLane].distanceFromCenter, lanes[currentStartLane].distanceFromCenter) - lastPosition);
                        //                        PrintLine("Last position: " + lastPosition);
                        //                        PrintLine("Position: " + position);
                        //                        PrintLine("Current velocity: " + currentVelocity);
                        //                        PrintLine("Velocity based on positions: " + positionVelocity);
                        //                        PrintLine("Lane switch from " + lastStartLane + " to " + currentStartLane);
                        //                        PrintLine("Start lane offset " + lanes[lastStartLane].distanceFromCenter + " end: " + startLaneOffset);
                        //                        PrintLine("Last length: " + lastPiece.length);
                        //                        PrintLine("Last angle: " + lastPiece.angle);
                        //                        PrintLine("Last radius: " + lastPiece.radius);
                        //                    }
                        //                    PrintLine ("Velocity based on pos: " + velocity);

                        //switchLanes = laneSwitches [pieceId]; // Issued later

                        if (pieceId == 0)
                        {
                            fullOnTurbo = false;
                        }
    					
                        if (pieces[pieceId].@switch)
                        {
                            carsToAvoidBehind.Clear();
                            carsToAvoid.Clear(); // Reset cars to avoid
                        }
    					
                        /*if (lastPiece.radius > 0.0 && pieces [pieceId].radius == 0) {
                            targetVelocity += 1.0;
                            PrintLine ("Increased velocity to : " + targetVelocity);
                        }*/
                    }
                    double acceleration = velocity - lastVelocity;
    				
                    // Calculate engine power
                    if (!enginePowerCalculated && velocity > 0)//carPositions.gameTick == 1)
                    {
                        enginePowerCalculated = true;
                        double oldEnginePower = enginePower;
                        enginePower = velocity;
                        PrintLine("Dynamically calculated engine power to be: " + enginePower);
                    }
    				
                    // Calculate friction
                    //newVelocity = oldVelocity * friction + throttle * enginePower
                    //newVelocity - throttle * enginePower = oldVelocity * friction
                    //friction = (newVelocity - throttle * enginePower) / oldVelocity
                    if (!frictionCalculated && enginePowerCalculated && velocity > 0 && lastVelocity > 0)//carPositions.gameTick == 2)
                    {
                        frictionCalculated = true;
                        double oldFriction = friction;
                        // Took away the turbo factor to minimize risks for bugs
                        //friction = (velocity - lastThrottle * enginePower * currentTurboFactor) / lastVelocity;
                        friction = (velocity - lastThrottle * enginePower) / lastVelocity;
                        PrintLine("Dynamically calculated friction to be: " + friction);
                    }

                    double calculatedVelocity = (lastVelocity * friction + lastThrottle * enginePower * lastTurboFactor);

                    bumped = false;
                    if (!lastBumped && (velocity == 0 && Math.Abs(calculatedVelocity - velocity) > 0.1 || Math.Abs(1.0 - velocity / calculatedVelocity) > 0.05) && lastVelocity > 0)
                    {
                        PrintLine("Big discrepancy in our velocity and our prediction. Assuming BUMP or CRASH.");
                        PrintLine("We assume we should be going at " + calculatedVelocity + " but we seem to be going at " + velocity);
                        bumped = true;
                        if (closestEnemyVelocity >= 0.0)
                        {
                            PrintLine("Assuming we were bumped by enemy driving at speed " + closestEnemyVelocity);
                            velocity = closestEnemyVelocity * 0.9;
                            PrintLine("Assuming our real speed to be + " + velocity);
                        }
                    }

                    if (/*calculatedSafeSpeedFactors.Count < 100 && */lastAngle != 0.0 && lastVelocity > 0.0 && lastStartLane == lastEndLane && !bumped)
                    {
                        // Derived from drift angle prediction formula
                        //double coefficient = 0.3;
                        double radius = lastPiece.getRadius(lanes[lastStartLane].distanceFromCenter, lanes[lastEndLane].distanceFromCenter, lastPosition);
                        double ddAngle = (currentAngle - lastAngle) - (lastAngle - lastLastAngle);
                        double dAngle = lastAngle - lastLastAngle;
                        ddAngles.Add(ddAngle);
                        radii.Add(radius);
                        velocities.Add(lastVelocity);
                        angles.Add(lastAngle);
                        dAngles.Add(dAngle);

                        if (lastVelocity > 0.0 && lastPiece.angle == 0)
                        {
                            if (ddAngles.Count > 1)
                            {
                                int index = ddAngles.Count - 2;
                                for (index = ddAngles.Count - 2; index >= 0; --index)
                                {
                                    if (velocities[index] > 0.0 && radii[index] == 0)
                                    {
                                        break;
                                    }
                                }
                                if (index >= 0)
                                {
                                    double prevDD = ddAngles[index];
                                    double prevVelAngle = velocities[index] * angles[index];
                                    double prevDAngle = dAngles[index];

                                    double dAngleCoeff = (prevDD * lastVelocity * lastAngle - ddAngle * prevVelAngle) /
                                                         (prevDAngle * lastVelocity * lastAngle - dAngle * prevVelAngle);
                                    double angleCoeff = (dAngle * prevDD - prevDAngle * ddAngle) /
                                                        (dAngle * prevVelAngle - prevDAngle * lastVelocity * lastAngle);

                                    calculatedDAngleCoefficients.Add(dAngleCoeff);
                                    calculatedAngleCoefficients.Add(angleCoeff);

                                    calculatedAngleCoefficients.Sort();
                                    calculatedDAngleCoefficients.Sort();

                                    angleCoefficient = calculatedAngleCoefficients[calculatedAngleCoefficients.Count / 2];
                                    dAngleCoefficient = calculatedDAngleCoefficients[calculatedDAngleCoefficients.Count / 2];

                                    //                                if (calculatedDAngleCoefficients.Count % 10 == 1)
                                    {
                                        PrintLine("Calculated dAngleCoeff to be: " + dAngleCoeff);
                                        PrintLine("Calculated angleCoeff to be: " + angleCoeff);
                                        PrintLine("Median dAngleCoeff: " + dAngleCoefficient);
                                        PrintLine("Median angleCoeff: " + angleCoefficient);
                                    }
                                }
                            }
                        }

                        if ((lastLastAngle == 0 || /*lastVelocity > Math.Sqrt(safeSpeedFactor * radius) &&*/ calculatedAngleCoefficients.Count > 0) && lastPiece.radius > 0)
                        {
                            
                            double G = dAngleCoefficient * (lastAngle - lastLastAngle) + angleCoefficient * (lastAngle * lastVelocity);

                            double D = Math.Abs(ddAngle - G);

                            if (D > 0.001)
                            {

                                double factor = Math.Pow((speedCoefficient * lastVelocity * lastVelocity) / (D + speedCoefficient * lastVelocity), 2.0) / radius;

                                calculatedSafeSpeedFactors.Add(factor);
                                calculatedSafeSpeedFactors.Sort();

                                safeSpeedFactor = calculatedSafeSpeedFactors[calculatedSafeSpeedFactors.Count / 2];

                                double coefficient = 0;

                                if (ddAngles.Count > 1)
                                {
                                    int index = ddAngles.Count - 2;
                                    for (index = ddAngles.Count - 2; index >= 0; --index)
                                    {
                                        if (radii[index] > 0 && velocities[index] > Math.Sqrt(safeSpeedFactor * radii[index]))
                                        {
                                            break;
                                        }
                                    }
                                    if (index >= 0)
                                    {
                                        double prevDD = Math.Abs(ddAngles[index] - (dAngleCoefficient * dAngles[index] + angleCoefficient * angles[index] * velocities[index]));
                                        double prevRadius = radii[index];
                                        double prevVelocity = velocities[index];
                                        coefficient = (prevDD * prevRadius * prevVelocity * Math.Pow(lastVelocity, 4.0) - D * radius * lastVelocity * Math.Pow(prevVelocity, 4.0) -
                                        Math.Sqrt(Math.Pow(prevDD, 2.0) * radius * prevRadius * Math.Pow(prevVelocity, 4.0) * Math.Pow(lastVelocity, 6.0) -
                                        2 * D * prevDD * radius * prevRadius * Math.Pow(lastVelocity * prevVelocity, 5.0) +
                                        Math.Pow(D, 2.0) * radius * prevRadius * Math.Pow(prevVelocity, 6.0) * Math.Pow(lastVelocity, 4.0))) /
                                        (radius * Math.Pow(lastVelocity, 2.0) * Math.Pow(prevVelocity, 4.0) - prevRadius * Math.Pow(lastVelocity, 4.0) * Math.Pow(prevVelocity, 2.0));
                                        //PrintLine("Coeff: " + coefficient);
                                        double alt_coefficient = (prevDD * prevRadius * prevVelocity * Math.Pow(lastVelocity, 4.0) - D * radius * lastVelocity * Math.Pow(prevVelocity, 4.0) +
                                                                 Math.Sqrt(Math.Pow(prevDD, 2.0) * radius * prevRadius * Math.Pow(prevVelocity, 4.0) * Math.Pow(lastVelocity, 6.0) -
                                                                 2 * D * prevDD * radius * prevRadius * Math.Pow(lastVelocity * prevVelocity, 5.0) +
                                                                 Math.Pow(D, 2.0) * radius * prevRadius * Math.Pow(prevVelocity, 6.0) * Math.Pow(lastVelocity, 4.0))) /
                                                                 (radius * Math.Pow(lastVelocity, 2.0) * Math.Pow(prevVelocity, 4.0) - prevRadius * Math.Pow(lastVelocity, 4.0) * Math.Pow(prevVelocity, 2.0));
                                        //PrintLine("ALT speed coefficient: " + alt_coefficient);
                                        if ((radius * Math.Pow(lastVelocity, 2.0) * Math.Pow(prevVelocity, 4.0) - prevRadius * Math.Pow(lastVelocity, 4.0) * Math.Pow(prevVelocity, 2.0)) > 0.0)//coefficient < 0.0)
                                        {
                                            coefficient = alt_coefficient;
                                        }

                                        calculatedSpeedCoefficients.Add(coefficient);
                                        calculatedSpeedCoefficients.Sort();

                                        speedCoefficient = calculatedSpeedCoefficients[calculatedSpeedCoefficients.Count / 2];

                                        //                            PrintLine("Using speed coefficient: " + speedCoefficient);

                                        if (coefficient > speedCoefficient + 0.01)
                                        {
                                            PrintLine("CALCULATED HIGHER THAN MEDIAN SPEED COEFFICIENT: " + coefficient);
                                        }
                                        if (coefficient < speedCoefficient - 0.01)
                                        {
                                            PrintLine("CALCULATED LOWER THAN MEDIAN SPEED COEFFICIENT: " + coefficient);
                                        }
                                    }
                                }



                                //                        if (calculatedSafeSpeedFactors.Count % 10 == 1)
                                {
                                    PrintLine("Sample safe speed factor: " + factor);
                                    PrintLine("Sample speed coefficient: " + coefficient);
                                    PrintLine("Safe speed factor median: " + safeSpeedFactor);
                                    PrintLine("Speed coefficient median: " + speedCoefficient);
                                }
                                if (factor > safeSpeedFactor + 0.01)
                                {
                                    PrintLine("CALCULATED HIGHER THAN MEDIAN FACTOR");
                                }
                                if (factor < safeSpeedFactor - 0.01)
                                {
                                    PrintLine("CALCULATED LOWER THAN MEDIAN FACTOR");
                                }
                                //                    }
                                if (calculatedSafeSpeedFactors.Count == 100)
                                {
                                    PrintLine("Calculated safe speed factor to be: " + safeSpeedFactor + " (FINAL)");
                                    PrintLine("Calculated speed coefficient to be: " + speedCoefficient + " (FINAL)");
                                }
                            }
                            else
                            {
                                PrintLine("Assuming we're not going fast enough to alter the angle.");
                            }
                        }
                        
                        //double angularVelocity = velocity/radius * 180.0 / Math.PI;
                        //double Bslip = angularVelocity - Math.Abs(currentAngle);
                        //double Vthres = Bslip * Math.PI / 180.0 * radius;
                        //double Fslip = Vthres * Vthres / radius;
                        //PrintLine("Calculated using other method to be: " + Fslip + ". B=" + angularVelocity + ", Bslip=" + Bslip + ", Vthres=" + Vthres);
                    }

                    if (lastStartLane >= 0 && lastEndLane >= 0)
                    {
                        double lastRadius = lastPiece.getRadius(lanes[lastStartLane].distanceFromCenter, lanes[lastEndLane].distanceFromCenter, lastPosition);
                        if (calculatedSafeSpeedFactors.Count == 0 &&
                            lastAngle == 0 && currentAngle == 0 &&
                            lastRadius > 0 && lastVelocity > Math.Sqrt(safeSpeedFactor * lastRadius))
                        {
                            safeSpeedFactor = lastVelocity * lastVelocity / lastRadius;
                            PrintLine("Overriding safe speed factor: " + safeSpeedFactor);
                        }
                    }

                    if (Math.Abs(currentAngle) > Math.Abs(largestAngle))
                    {
                        largestAngle = currentAngle;
                        //PrintLine ("Largest angle: " + largestAngle + " on piece " + pieceId);
                    }

                    int closestEnemyFrontPieceNum = -1;
                    int closestEnemyBehindPieceNum = -1;
                    double closestEnemyFrontPieceDistance = 0.0;
                    double closestEnemyBehindPieceDistance = 0.0;
                    double closestEnemyFrontVelocity = 0.0;
                    double closestEnemyBehindVelocity = 0.0;
                    PiecePosition closestEnemyFront = null;
                    PiecePosition closestEnemyBehind = null;

                    double playerPieceDistance = currentPiece.getLength(startLaneOffset, endLaneOffset) - myCar.piecePosition.inPieceDistance;

                    double myRankPoints = CalculateRankPoints(myCar);               
                    targetLimitMod = 0;
                    foreach (CarPosition enemyCarPos in carPositions.data)
                    {
                        if (enemyCarPos == myCar)
                            continue;

                        string enemyColor = enemyCarPos.id.color;

                        // Calculate my rank
                        double enemyPoints = CalculateRankPoints(enemyCarPos);
                        if (enemyPoints > myRankPoints)
                        {
                            myRank++;
                        }

                        // Search for the last enemy car pos
                        CarPosition lastEnemyCarPos = null;
                        if (lastCarPositions != null)
                        {
                            foreach (CarPosition lastEnemy in lastCarPositions.data)
                            {
                                if (enemyColor == lastEnemy.id.color)
                                {
                                    lastEnemyCarPos = lastEnemy;
                                }
                            }
                        }

                        PiecePosition enemyPiecePos = enemyCarPos.piecePosition;

                        double enemyStartLaneOffset = lanes[enemyPiecePos.lane.startLaneIndex].distanceFromCenter;
                        double enemyEndLaneOffset = lanes[enemyPiecePos.lane.endLaneIndex].distanceFromCenter;

                        // Check if the enemy is moving
                        bool enemyMoving = false;
                        double enemyVelocity = -1.0;
                        if (lastEnemyCarPos != null &&
                            (enemyPiecePos.pieceIndex != lastEnemyCarPos.piecePosition.pieceIndex ||
                            enemyPiecePos.inPieceDistance != lastEnemyCarPos.piecePosition.inPieceDistance))
                        {
                            enemyMoving = true;

                            // TODO: calculate it better
                            if (enemyPiecePos.pieceIndex == lastEnemyCarPos.piecePosition.pieceIndex)
                            {
                                enemyVelocity = enemyPiecePos.inPieceDistance - lastEnemyCarPos.piecePosition.inPieceDistance;
                            }
                            else
                            {
                                enemyVelocity = enemyPiecePos.inPieceDistance + pieces[enemyPiecePos.pieceIndex].getLength(enemyStartLaneOffset, enemyEndLaneOffset) - lastEnemyCarPos.piecePosition.inPieceDistance;
                            }
                        }
                        else
                        {
                            enemyVelocity = 0.0;
                        }

                        // Check if the enemy is alive again
                        if (crashedEnemies.Contains(enemyColor))
                        {
                            if (enemyMoving)
                            {
                                //PrintLine("Crashed enemy was found to be moving! removing from the list!");
                                crashedEnemies.Remove(enemyColor);
                            }
                        }

                        // Check if the enemy should be put into the crashed list
                        if (!enemyMoving && !crashedEnemies.Contains(enemyColor))
                        {
                            //PrintLine("Enemy not moving! Putting to the crashed list!");
                            crashedEnemies.Add(enemyColor);
                        }

                        // Check if we should pass the enemy using a different lane
                        if ((pieces[(pieceId + 1) % pieces.Length].@switch || pieces[(pieceId + 2) % pieces.Length].@switch)
                            &&
                            (!enemyMoving || enemyVelocity >= 0.0))
                        {
                            if (enemyPiecePos.pieceIndex == (myCar.piecePosition.pieceIndex + 1) % pieces.Length && (enemyVelocity < velocity - 0.5 || !enemyMoving) ||
                                enemyPiecePos.pieceIndex == (myCar.piecePosition.pieceIndex + 2) % pieces.Length && (enemyVelocity < velocity - 1.5 || !enemyMoving) ||
                                enemyPiecePos.pieceIndex == (myCar.piecePosition.pieceIndex + 3) % pieces.Length && (enemyVelocity < velocity - 2.5 || !enemyMoving) ||
                                enemyPiecePos.pieceIndex == (myCar.piecePosition.pieceIndex + 4) % pieces.Length && (enemyVelocity < velocity - 3.5 || !enemyMoving) ||
                                enemyPiecePos.pieceIndex == (myCar.piecePosition.pieceIndex + 5) % pieces.Length && (enemyVelocity < velocity - 4.5 || !enemyMoving) ||
                                enemyPiecePos.pieceIndex == (myCar.piecePosition.pieceIndex + 6) % pieces.Length && (enemyVelocity < velocity - 5.5 || !enemyMoving) ||
                                enemyPiecePos.pieceIndex == (myCar.piecePosition.pieceIndex + 7) % pieces.Length && (enemyVelocity < 1.0 || !enemyMoving))
                            {
                                if (!carsToAvoid.Contains(enemyCarPos.id.color))
                                {
                                    carsToAvoid.Add(enemyCarPos.id.color);
                                    PrintLine("Trying to avoid " + enemyCarPos.id.color + " car (" + enemyCarPos.id.name + ")");
                                }
                                //lanesToAvoid.Add(enemyPiecePos.lane.endLaneIndex);
                                //PrintLine("Trying to avoid lane " + enemyPiecePos.lane.endLaneIndex);
                            }
                        }

                        // Check if we should slow down to avoid collision with an enemy
                        if (!crashedEnemies.Contains(enemyColor))
                        {
                            if (enemyPiecePos.lane.endLaneIndex == myCar.piecePosition.lane.endLaneIndex)
                            {
                                double enemyPieceDistance = pieces[enemyPiecePos.pieceIndex].getLength(enemyStartLaneOffset, enemyEndLaneOffset) - enemyPiecePos.inPieceDistance;
                                int frontPieces = enemyPiecePos.pieceIndex - myCar.piecePosition.pieceIndex;
                                if (frontPieces < 0 ||
                                    frontPieces == 0 && enemyPieceDistance > playerPieceDistance)
                                {
                                    frontPieces += pieces.Length;
                                }
                                if (closestEnemyFrontPieceNum == -1 ||
                                    closestEnemyFrontPieceNum > frontPieces ||
                                    closestEnemyFrontPieceNum == frontPieces && closestEnemyFrontPieceDistance < enemyPieceDistance)
                                {
                                    closestEnemyFrontPieceNum = frontPieces;
                                    closestEnemyFrontPieceDistance = enemyPieceDistance;
                                    closestEnemyFrontVelocity = enemyVelocity;
                                    closestEnemyFront = enemyPiecePos;
                                }

                                int behindPieces = myCar.piecePosition.pieceIndex - enemyPiecePos.pieceIndex;
                                if (behindPieces < 0 ||
                                    behindPieces == 0 && enemyPieceDistance < playerPieceDistance)
                                {
                                    behindPieces += pieces.Length;
                                }
                                if (closestEnemyBehindPieceNum == -1 ||
                                    closestEnemyBehindPieceNum > behindPieces ||
                                    closestEnemyBehindPieceNum == behindPieces && closestEnemyBehindPieceDistance > enemyPieceDistance)
                                {
                                    closestEnemyBehindPieceNum = behindPieces;
                                    closestEnemyBehindPieceDistance = enemyPieceDistance;
                                    closestEnemyBehindVelocity = enemyVelocity;
                                    closestEnemyBehind = enemyPiecePos;
                                }
                            }
                        }

                        // Check if we should ram the enemy                 
                        if ((enemyPiecePos.pieceIndex == myCar.piecePosition.pieceIndex + 1 ||
                            (enemyPiecePos.pieceIndex == myCar.piecePosition.pieceIndex &&
                            enemyPiecePos.inPieceDistance > myCar.piecePosition.inPieceDistance)) &&
                            enemyPiecePos.lane.endLaneIndex == myCar.piecePosition.lane.endLaneIndex)
                        {
                            //throttle = 1.0;
                            //PrintLine("Ramming!");
                        }
                    }

                    // TODO: Take into account the length of the car when predicting collisions.
                    targetLimitMod = 0;
                    double frontEnemyTimeToCollision = -1.0;
                    double behindEnemyTimeToCollision = -1.0;
                    if (closestEnemyFrontPieceNum >= 0 && velocity > closestEnemyFrontVelocity)
                    {
                        double frontEnemyDistance = 0.0;
                        if (closestEnemyFrontPieceNum == 0)
                        {
                            frontEnemyDistance = playerPieceDistance - closestEnemyFrontPieceDistance;
                        }
                        else
                        {
                            frontEnemyDistance += currentPiece.getLength(startLaneOffset, endLaneOffset) - myCar.piecePosition.inPieceDistance;
                            frontEnemyDistance += pieces[(pieceId + closestEnemyFrontPieceNum) % pieces.Length].getLength(endLaneOffset, endLaneOffset) - closestEnemyFrontPieceDistance;

                            for (int id = pieceId + 1; id < pieceId + closestEnemyFrontPieceNum; ++id)
                            {
                                frontEnemyDistance += pieces[id % pieces.Length].getLength(endLaneOffset, endLaneOffset);
                            }
                        }

                        frontEnemyDistance -= carLength;
                        frontEnemyDistance = Math.Max(frontEnemyDistance, 0.0);

                        frontEnemyTimeToCollision = Math.Max(frontEnemyDistance / (velocity - closestEnemyFrontVelocity), 0.0);
                        PrintLine("Time to collision: " + frontEnemyTimeToCollision);

                        if (frontEnemyTimeToCollision < 60)
                        {
                            targetLimitMod -= Math.Min(30.0 - frontEnemyTimeToCollision / 2, 30.0);
                            PrintLine("Reduced target limit mod to " + targetLimitMod + " because of car in front");
                        }
                    }
                    if (closestEnemyBehindPieceNum >= 0 && velocity < closestEnemyBehindVelocity)
                    {
                        double behindEnemyDistance = 0.0;
                        if (closestEnemyBehindPieceNum == 0)
                        {
                            behindEnemyDistance = closestEnemyBehindPieceDistance - playerPieceDistance;
                        }
                        else
                        {
                            behindEnemyDistance += myCar.piecePosition.inPieceDistance;
                            Piece enemyPiece = pieces[(pieceId + pieces.Length - closestEnemyBehindPieceNum) % pieces.Length];
                            behindEnemyDistance += closestEnemyBehindPieceDistance;

                            for (int id = pieceId - 1 + pieces.Length; id > pieceId + pieces.Length - closestEnemyBehindPieceNum; --id)
                            {
                                behindEnemyDistance += pieces[id % pieces.Length].getLength(endLaneOffset, endLaneOffset);
                            }
                        }

                        behindEnemyDistance -= carLength;
                        behindEnemyDistance = Math.Max(behindEnemyDistance, 0.0);

                        behindEnemyTimeToCollision = Math.Max(behindEnemyDistance / (closestEnemyBehindVelocity - velocity), 0.0);
                        PrintLine("Time to collision: " + behindEnemyTimeToCollision);

                        if (behindEnemyTimeToCollision < 60)
                        {
                            targetLimitMod -= Math.Min(30.0 - behindEnemyTimeToCollision / 2, 30.0);
                            PrintLine("Reduced target limit mod to " + targetLimitMod + " because of car behind");
                        }
                    }

                    if (behindEnemyTimeToCollision >= 0 && behindEnemyTimeToCollision < 10 && (behindEnemyTimeToCollision < frontEnemyTimeToCollision || frontEnemyTimeToCollision < 0))
                    {
                        closestEnemyVelocity = closestEnemyBehindVelocity;
                    }
                    else if (frontEnemyTimeToCollision >= 0 && frontEnemyTimeToCollision < 10 && (frontEnemyTimeToCollision < behindEnemyTimeToCollision || behindEnemyTimeToCollision < 0))
                    {
                        closestEnemyVelocity = closestEnemyFrontVelocity;
                    }
                    else
                    {
                        closestEnemyVelocity = -1.0;
                    }
                      

                    angleLimitMod = targetLimitMod; //Math.Max(targetLimitMod, targetLimitMod * 0.2 + angleLimitMod * 0.8);

                    if (angleLimitMod < -0.01)
                    {
                        PrintLine("Driving with angle limit reduced by " + angleLimitMod + ", target: " + targetLimitMod);
                    }

                    if (startingRank == -1)
                    {
                        startingRank = myRank;
                        //PrintLine("Starting rank was " + myRank);
                    }

                    // Check if switch lanes needs to be overridden
                    if (position > 0.5 * pieces[pieceId].getLength(startLaneOffset, endLaneOffset) &&
                        lastPosition <= 0.5 * pieces[pieceId].getLength(startLaneOffset, endLaneOffset) &&
                        calculatedSafeSpeedFactors.Count > 0)
                    {
                        switchLanes = laneSwitches[pieceId];
                    }

                    if (switchLanes != 0)
                    {
                        // Generate the lanes to avoid on the fly
                        List<int> lanesToAvoid = new List<int>();
                        for (int i = 0; i < carsToAvoid.Count; i++)
                        {
                            foreach (CarPosition enemyCarPos in carPositions.data)
                            {
                                if (enemyCarPos.id.color == carsToAvoid[i])
                                {
                                    int laneToAvoid = enemyCarPos.piecePosition.lane.endLaneIndex;
                                    lanesToAvoid.Add(laneToAvoid);
                                    PrintLine("Avoiding lane " + laneToAvoid + " because of " + carsToAvoid[i] + " car (" + enemyCarPos.id.name + ")");
                                }
                            }
                        }

                        // Can't do the switch?
                        if (currentEndLane == 0 && switchLanes == -1 ||
                            currentEndLane == lanes.Length - 1 && switchLanes == 1)
                        {
                            switchLanes = 0;
                            //PrintLine("Illegal switch!");
                        }

                        // Should avoid the resulting lane?

                        if (lanesToAvoid.Contains(currentEndLane + switchLanes))
                        {
                            if (!lanesToAvoid.Contains(currentEndLane))
                            {
                                switchLanes = 0;
                                //PrintLine("Lane switch overridden, current lane is fine");
                            }
                            else if (currentEndLane - 1 >= 0 && !lanesToAvoid.Contains(currentEndLane - 1))
                            {
                                switchLanes = -1;
                                //PrintLine("Switching left to faster lane");
                            }
                            else if (currentEndLane + 1 < lanes.Length && !lanesToAvoid.Contains(currentEndLane + 1))
                            {
                                switchLanes = 1;
                                //PrintLine("Switching right to faster lane");
                            }
                            else if (currentEndLane - 2 >= 0 && !lanesToAvoid.Contains(currentEndLane - 2))
                            {
                                switchLanes = -1;
                                //PrintLine("Switching left to switch left again to faster lane");
                            }
                            else if (currentEndLane + 2 < lanes.Length && !lanesToAvoid.Contains(currentEndLane + 2))
                            {
                                switchLanes = 1;
                                //PrintLine("Switching right to switch right again to faster lane");
                            }
                            else
                            {
                                //PrintLine("All lanes stuck! Using the normal optimal strategy");
                            }
                        }
                        else
                        {                       
                            //PrintLine("Switch fine!");
                        }

                        //PrintLine("Switching from lane " + myLane + " to lane " + (myLane + switchLanes));

                        if (switchLanes == 0)
                        {
                            nextSwitch = Switch.NoSwitch;
                        }
                        else if (switchLanes > 0)
                        {
                            nextSwitch = Switch.Right;
                        }
                        else
                        {
                            nextSwitch = Switch.Left;
                        }
                    }

                    double debugPredictedFutureAngle = 0.0;
                    bool wasSafe = false;
                    bool wasHardSafe = false;

                    int numThrottles = (currentTurboFactor > 1.0 ? 3 : 2);
                    double[] throttles = new double[numThrottles];
                    int[] safeTicks = new int[numThrottles];
                    int[] hardSafeTicks = new int[numThrottles];
                    double[] bestMaxAngles = new double[numThrottles];
                    for (int i = 0; i < numThrottles; ++i)
                    {
                        throttles[i] = (double)i / (double)(numThrottles - 1);
                        safeTicks[i] = 0;
                        hardSafeTicks[i] = 0;
                        bestMaxAngles[i] = -1;
                    }
                    throttles[1] = 1.0 / currentTurboFactor;
                    //                { 0.0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1.0 };
                    //                int[] safeTicks = new int[]        { 0,    0,   0,    0,   0,    0,   0,    0,   0,    0,   0,    0,   0,    0,   0,    0,   0,    0,   0,    0,   0 };
                    //                double[] throttles = new double[]{ 0.0, 1.0 };
                    //                int[] safeTicks = new int[]        { 0,   0};

                    for (int throttleIndex = throttles.Length - 1; throttleIndex >= 0; throttleIndex--)
                    {
                        for (int throttleFrames = 20; throttleFrames > 0; --throttleFrames)
                        {
                            wasSafe = true;
                            wasHardSafe = true;

                            throttle = throttles[throttleIndex];
                            double futureVelocity = velocity;// * friction + enginePower * currentTurboFactor;
                            double futureAngle = currentAngle;
                            double futureDAngle = angleDiff;
                            int futurePieceId = pieceId;

                            double futureRadius = pieces[futurePieceId].getRadius(startLaneOffset, endLaneOffset, position) * Math.Sign(pieces[futurePieceId].angle);
                            double futurePosition = position;
                            int futureStartLane = currentStartLane;
                            int futureEndLane = currentEndLane;
                            double futureStartLaneOffset = startLaneOffset;
                            double futureEndLaneOffset = endLaneOffset;
                            int futureTurboTicksLeft = turboTicksLeft;
                            double futureTurboFactor = currentTurboFactor;
                            double futureAngleLimit = pieceAngleLimits[futurePieceId];
                            bool futureHasSwitched = false;
                            double maxAngle = 0.0;
                            double A = speedCoefficient;
                            double B = dAngleCoefficient;
                            double C = angleCoefficient;
                            for (int i = 0; i < 200; ++i)
                            {
                                //double nextDDAngle = A * (Math.Abs (futureRadius) > 0.0 ? Math.Max (futureVelocity * futureVelocity / Math.Abs (futureRadius) - safeSpeedFactor, 0.0) * Math.Sign (futureRadius) * Math.Pow(Math.Abs(futureRadius), 0.33) : 0.0) / Math.Pow(190.0, 0.33) +
                                double nextDDAngle = A * (Math.Abs(futureRadius) > 0.0 ? Math.Max(futureVelocity * (futureVelocity - Math.Sqrt(safeSpeedFactor * Math.Abs(futureRadius))) / Math.Sqrt(safeSpeedFactor * Math.Abs(futureRadius)), 0) * Math.Sign(futureRadius) : 0.0) +
                                                     B * futureDAngle +
                                                     C * futureAngle * futureVelocity;
                                futureDAngle += nextDDAngle;
                                futureAngle += futureDAngle;
                                //if (i == 0)
                                //	Print("Future angle: " + (futureAngle) + "\t");
                                if (i == 0 && throttleFrames == 0)
                                    debugPredictedFutureAngle = futureAngle;

                                if (Math.Abs(futureAngle) >= futureAngleLimit + angleLimitMod)
                                { //3.0) {
                                    //                            if (i <= 1)
                                    //                            {
                                    //                                PrintLine("Predicting crash in " + i + " + ticks, with angle going up to " + (futureAngle));
                                    //                            }
                                    wasSafe = false;
                                    //break;
                                }
                                if (Math.Abs(futureAngle) >= hardAngleLimit)
                                {
                                    wasHardSafe = false;
                                }
                                if (Math.Abs(futureAngle) > maxAngle)
                                {
                                    maxAngle = Math.Abs(futureAngle);
                                }

                                if (i < throttleFrames)
                                    futureVelocity = futureVelocity * friction + throttle * enginePower * futureTurboFactor; // Assuming 1.0 throttle for next two ticks as well
                                else
                                    futureVelocity = futureVelocity * friction; // Assuming 0.0 throttle here
    					               
                     
                                if (futureTurboTicksLeft > 0)
                                {
                                    --futureTurboTicksLeft;
                                }
                                else
                                {
                                    futureTurboFactor = 1.0;
                                }
                        
                                futurePosition += futureVelocity;
                                while (futurePosition >= pieces[futurePieceId].getLength(futureStartLaneOffset, futureEndLaneOffset))
                                {
                                    futurePosition -= pieces[futurePieceId].getLength(futureStartLaneOffset, futureEndLaneOffset);
                                    futurePieceId = (futurePieceId + 1) % pieces.Length;
                                    if (pieces[futurePieceId].@switch)
                                    {
                                        futureHasSwitched = true;
                                    }
                                    futureStartLane = futureEndLane;
                                    int laneSwitch = laneSwitches[(futurePieceId + laneSwitches.Length - 1) % laneSwitches.Length];
                                    if (nextSwitch != Switch.Undecided && futurePieceId == pieceId + 1)
                                    {
                                        if (nextSwitch == Switch.Left)
                                        {
                                            laneSwitch = -1;
                                        }
                                        else if (nextSwitch == Switch.Right)
                                        {
                                            laneSwitch = 1;
                                        }
                                        else if (nextSwitch == Switch.NoSwitch)
                                        {
                                            laneSwitch = 0;
                                        }
                                    }
                                    futureEndLane = futureEndLane + laneSwitch;
                                    if (futureEndLane >= lanes.Length)
                                    {
                                        futureEndLane = lanes.Length - 1;
                                    }
                                    else if (futureEndLane < 0)
                                    {
                                        futureEndLane = 0;
                                    }
                                    futureStartLaneOffset = lanes[futureStartLane].distanceFromCenter;
                                    futureEndLaneOffset = lanes[futureEndLane].distanceFromCenter;

                                    if (pieceAngleLimits[futurePieceId] < futureAngleLimit)
                                    {
                                        futureAngleLimit = Math.Min(futureAngleLimit, pieceAngleLimits[futurePieceId]);
                                    }
                                }
                        
                                if (futureHasSwitched && nextSwitch == Switch.Undecided)
                                {
                                    double minRadius = -1;
                                    double maxRadius = -1;
                                    foreach (Lane lane in lanes)
                                    {
                                        //double radius = pieces[futurePieceId].getRadius(lane.distanceFromCenter, lane.distanceFromCenter, 0.0);
                                        double radius = pieces[futurePieceId].getRadius(futureStartLaneOffset, lane.distanceFromCenter, futurePosition);
                                        if (minRadius == -1 || radius < minRadius)
                                        {
                                            minRadius = radius;
                                        }
                                        if (maxRadius == -1 || radius > maxRadius)
                                        {
                                            maxRadius = radius;
                                        }
                                    }
                                    if (Math.Sign(pieces[futurePieceId].angle) == Math.Sign(futureDAngle))
                                    {
                                        futureRadius = minRadius;
                                    }
                                    else
                                    {
                                        futureRadius = maxRadius;
                                    }
    							
                                    futureRadius = futureRadius * Math.Sign(pieces[futurePieceId].angle);
                                }
                                else
                                {
                                    futureRadius = pieces[futurePieceId].getRadius(futureStartLaneOffset, futureEndLaneOffset, futurePosition) * Math.Sign(pieces[futurePieceId].angle);
                                }
                            }
                            if (maxAngle < bestMaxAngles[throttleIndex] || bestMaxAngles[throttleIndex] == -1)
                            {
                                bestMaxAngles[throttleIndex] = maxAngle;
                            }
                            if (wasHardSafe && hardSafeTicks[throttleIndex] == 0)
                            {
                                hardSafeTicks[throttleIndex] = throttleFrames;
                            }
                            if (wasSafe)
                            {
                                safeTicks[throttleIndex] = throttleFrames;
                                break;
                            }
                        }
                        if (safeTicks[throttleIndex] > 2)
                        {
                            throttle = throttles[throttleIndex];
                            break;
                        }
                        else
                        {
                            throttle = -1.0;
                        }
                    }
                    if (throttle == -1.0)
                    {
                        int bestThrottle = -1;
                        int bestHardThrottle = -1;
                        int bestMaxAngle = 0;
                        for (int throttleIndex = 0; throttleIndex < throttles.Length; ++throttleIndex)
                        {
                            if (safeTicks[throttleIndex] > 0 && (bestThrottle == -1 || safeTicks[throttleIndex] > safeTicks[bestThrottle]))
                            {
                                bestThrottle = throttleIndex;
                            }
                            if (hardSafeTicks[throttleIndex] > 0 && (bestHardThrottle == -1 || hardSafeTicks[throttleIndex] > hardSafeTicks[bestHardThrottle]))
                            {
                                bestHardThrottle = throttleIndex;
                            }
                            if (bestMaxAngles[throttleIndex] < bestMaxAngles[bestMaxAngle])
                            {
                                bestMaxAngle = throttleIndex;
                            }
                        }
                        if (bestThrottle == -1)
                        {
                            throttle = throttles[bestMaxAngle];
                            PrintLine("WARNING: Can't stay within our angle limit; aiming for " + bestMaxAngles[bestMaxAngle] + " degrees");
//                            if (bestHardThrottle == -1)
//                            {
//                                PrintLine("ABANDON ALL HOPE! We're probably crashing");
//                                throttle = 0.0;
//                            }
//                            else
//                            {
//                                PrintLine("SITUATION LOOKS BAD. Can't stay within our angle limit, but we can stay within 60 degrees.");
//                                throttle = throttles[bestHardThrottle];
//                            }
                        }
                        else
                        {
                            throttle = throttles[bestThrottle];
                        }
                    }
                    PrintLine("Throttle: " + throttle);
                    //                if (velocity > 0) {
                    //                    PrintLine ("Throttle: " + throttle);
                    //                }
                    lastTurboFactor = currentTurboFactor;
                    if (turboTicksLeft > 0)
                    {
                        --turboTicksLeft;
                    }
                    else
                    {
                        currentTurboFactor = 1.0;
                    }
                    // > 2 so that it won't ever confuse the friction calculation
                    // Use turbo before the longest straight piece or right away if on the last lap and the longest straight start piece is passed already
                    if (carPositions.gameTick > 2 && hasTurbo && //throttle == lastThrottle &&
                        (pieceId == longestStraightPieceIndex ||
						pieceId == longestStraightPieceIndex + 1 ||
                        pieceId >= longestStraightPieceIndex && myCar.piecePosition.lap == numLaps - 1))
                    {
                        send(new Turbo(carPositions.gameTick));
                        turboUsed = true;
                        hasTurbo = false;
                        PrintLine("Trying to use turbo!");
                    }
    				
                    // Don't slow down anymore when very near the goal
                    if (Math.Abs(currentAngle) < 20 &&
                        pieceId == pieces.Length - 2 &&
                        pieceId - 1 >= 0 &&
                        pieces[pieceId - 1].radius == 0 &&
                        pieces[pieceId].radius == 0 &&
                        myCar.piecePosition.lap == numLaps - 1)
                    {
                        fullOnTurbo = true;
                        PrintLine("FULL ON Turbo!");
                    }
        
                    // DEBUG!
                    //if (carPositions.gameTick < startingRank * 10)
                    //    throttle = 0.0;
                    //throttle = startingRank * 0.1;
                    //else
                    //throttle *= 0.4 + startingRank * 0.1;
                }
                lastLastThrottle = lastThrottle;

                if (carPositions.gameTick != -1)
                {
                    if (!turboUsed)
                    {
                        if (switchLanes != 0) //throttle == lastThrottle && 
                        {
                            send(new SwitchLane(switchLanes > 0 ? "Right" : "Left", carPositions.gameTick));
                            switchLanes = 0; // Reset switchlanes flag
                        }
                        else
                        {
                            if (fullOnTurbo)
                            {
                                //                            PrintLine ("TURBO");
                                send(new Throttle(1.0, carPositions.gameTick));
                                lastThrottle = 1.0;
                            }
                            else
                            {
                                send(new Throttle(throttle, carPositions.gameTick));
                                lastThrottle = throttle;
                            }
                        }
                    }
                    //lastThrottle = throttle;
                    lastVelocity = velocity;
                }
                else
                {
                    PrintLine("Gametick not given, so assuming start of a race");
                    lastThrottle = 0.0;
                    lastLastThrottle = 0.0;
                    lastVelocity = 0.0;
                }                
                lastPiece = pieces[pieceId];
                lastPosition = position;
                lastLastAngle = lastAngle;
                lastAngle = currentAngle;
                lastCarPositions = carPositions;
                lastBumped = bumped;

                lastStartLane = currentStartLane;
                lastEndLane = currentEndLane;

                raceLog.Add(new RaceLogEntry(velocity, currentAngle, pieces[pieceId].getRadius(startLaneOffset, endLaneOffset, position) * Math.Sign(pieces[pieceId].angle)));
	
                Print(carPositions.gameTick);
                Print("\t" + milliseconds + " ms");
                Print("\tRank " + myRank + " piece: " + pieceId);
                Print("(" + myCar.piecePosition.lap + ")");
                Print(" rad: " + Math.Round(pieces[pieceId].getRadius(startLaneOffset, endLaneOffset, position), 2));
                Print("\tLane: " + currentStartLane + "-" + currentEndLane);
                Print("\tv: " + velocity);//Math.Round(velocity, 3));
                Print("\tangle: " + currentAngle);//Math.Round(currentAngle, 4));
                //Print("\tpredicted next: " + debugPredictedFutureAngle);
                Print("\toutput: " + m_lastSentCommand);
                PrintLine("");
//                botOutput.WriteLine(m_lastSentCommand);
                milliseconds = stopwatch.ElapsedMilliseconds;
                break;
            case "join":
                PrintLine("Joined");
                PrintLine(line);
                break;
            case "gameEnd":
                lastPosition = 0.0;
             
                lastVelocity = 0.0;
                lastAngle = 0.0;
                lastLastAngle = 0.0;
                lastThrottle = 0.0;
                lastLastThrottle = 0.0;
                lastPiece = null;
                lastStartLane = -1;
                lastBumped = false;
                lastTurboFactor = 1.0;
                nextSwitch = Switch.Undecided;
             
                hasTurbo = false;
                currentTurboFactor = 1.0;
                turboTicksLeft = 0;
                fullOnTurbo = false;
             
                lastCarPositions = null;
                crashedEnemies.Clear();
                carsToAvoid.Clear();
                
                isCrashed = false;
                
                PrintLine("Race ended");
                PrintLine("Largest angle reached: " + largestAngle);
                PrintLine(line);
                largestAngle = 0.0;
				
				double reduceAmount = 0.5;
				PrintLine("Reducing angle limits globally by " + reduceAmount + " for the actual race");
				for (int i = 0; i < pieceAngleLimits.Length; i++)
				{
					pieceAngleLimits[i] -= reduceAmount;
				}

//                string jsonOutput = JsonConvert.SerializeObject(new RaceLog(raceLog.ToArray()));
//
//                System.IO.File.WriteAllText("raceLog.json", jsonOutput);
//
//                string csvOutput = "Car velocity, Curve radius, Car angle";
//                foreach (RaceLogEntry entry in raceLog)
//                {
//                    csvOutput += "\n" + entry.carVelocity + ", " + entry.curveRadius + ", " + entry.carAngle;
//                }
//                System.IO.File.WriteAllText("raceLog.csv", csvOutput);
//
//                botOutput.Flush();
//                serverInput.Flush();
//                debugOut.Flush();

                break;
            case "gameStart":
                PrintLine("Race starts");
                PrintLine(line);
				
                send(new Throttle(1.0, 0));
                break;
            case "spawn":
                Spawn spawn = JsonConvert.DeserializeObject<Spawn>(line);
                PrintLine(line);
             
                if (spawn.data.color == myCarColor)
                {
                    PrintLine("I'm back from the dead!");
                    
                    isCrashed = false;
                    hasTurbo = false;
                }
                break;
            case "crash":
                Crash crash = JsonConvert.DeserializeObject<Crash>(line);
                PrintLine(crash.data.color + " car (" + crash.data.name + ") crashed on tick " + crash.gameTick + " track piece " + pieceId);
                
                if (crash.data.color == myCarColor)
                {
                    double preCrashAngleLimit = pieceAngleLimits[pieceId];
                    pieceAngleLimits[pieceId] -= 2.0;
                    double postCrashAngleLimit = pieceAngleLimits[pieceId];
                    PrintLine("I CRASHED! Reducing angle limit for piece " + pieceId + " from " + preCrashAngleLimit + " to " + postCrashAngleLimit);
                    
                    crashCount++;
                    if (crashCount > 1)
                    {
                        PrintLine("Reducing angle limits globally by " + (crashCount - 1));
                        for (int i = 0; i < pieceAngleLimits.Length; i++)
                        {
                            pieceAngleLimits[i] -= crashCount - 1;
                        }
                    }

                    isCrashed = true;

//                    botOutput.Flush();
//                    serverInput.Flush();
//                    debugOut.Flush();
                }

                //targetVelocity = 5.0;
                break;
            case "turboAvailable":
                TurboAvailable turbo = JsonConvert.DeserializeObject<TurboAvailable>(line);
                if (!isCrashed)
                {
                    turboData = turbo.data;
                    hasTurbo = true;
                    PrintLine("Turbo available!");
                }
                else
                {
                    PrintLine("Missed a turbo because of a crash!");
                }
                break;
            case "turboStart":
                TurboStart turboStart = JsonConvert.DeserializeObject<TurboStart>(line);
                if (turboStart.data.color == myCarColor)
                {
                    currentTurboFactor = turboData.turboFactor;
                    turboTicksLeft = turboData.turboDurationTicks;
                    PrintLine("I used a turbo! " + turboStart.gameTick);
                }
                else
                {
                    PrintLine("Enemy " + turboStart.data.name + " used a turbo!");
                }
                break;

            case "turboEnd":
                TurboEnd turboEnd = JsonConvert.DeserializeObject<TurboEnd>(line);

                if (turboEnd.data.color == myCarColor)
                {
                    currentTurboFactor = 1.0;
                    turboTicksLeft = 0;
                    PrintLine("My turbo ended!");
                }
                else
                {
                    PrintLine("Enemy " + turboEnd.data.name + " turbo ended!");
                }
                break;
            case "lapFinished":
            case "finish":
                PrintLine(line);
//                botOutput.Flush();
//                serverInput.Flush();
//                debugOut.Flush();
                break;

            default:
                PrintLine(line);
                break;
            }
        }
    }

    private double CalculateRankPoints(CarPosition carPosition)
    {
        double points = carPosition.piecePosition.lap * 10000000.0;
        points += carPosition.piecePosition.pieceIndex * 10000.0;
        points += carPosition.piecePosition.inPieceDistance;
        return points;
    }

    private void send(SendMsg msg)
    {
        string json = msg.ToJson(sendTicks);
        PrintLine(json);
        writer.WriteLine(json);
        m_lastSentCommand = json;
    }
}

class MsgWrapper
{
    public string msgType;
    public Object data;
    public int gameTick;

    protected MsgWrapper()
    {
    }

    public MsgWrapper(string msgType, Object data, int gameTick)
    {
        this.msgType = msgType;
        this.data = data;
        this.gameTick = gameTick;
    }
}

class MsgWrapperTickless
{
    public string msgType;
    public Object data;

    protected MsgWrapperTickless()
    {
    }

    public MsgWrapperTickless(string msgType, Object data)
    {
        this.msgType = msgType;
        this.data = data;
    }
}

class ReceiveMsg : MsgWrapper
{
    public string gameId;

    public ReceiveMsg(string msgType, Object data, string gameId, int gameTick)
    {
        this.msgType = msgType;
        this.data = data;
        this.gameId = gameId;
        this.gameTick = gameTick;
    }
}

abstract class SendMsg
{
    public string ToJson(bool useTicks)
    {
        if (useTicks)
        {
            return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData(), this.MsgTick()));
        }
        else
        {
            return JsonConvert.SerializeObject(new MsgWrapperTickless(this.MsgType(), this.MsgData()));
        }
    }

    protected virtual Object MsgData()
    {
        return this;
    }

    protected abstract string MsgType();

    protected abstract int MsgTick();
}

class Join : SendMsg
{
    public string name;
    public string key;

    public Join(string name, string key)
    {
        this.name = name;
        this.key = key;
    }

    protected override string MsgType()
    {
        return "join";
    }

    protected override int MsgTick()
    {
        return 0;
    }
}

class BotId
{
    public string name;
    public string key;

    public BotId(string name, string key)
    {
        this.name = name;
        this.key = key;
    }
}

class JoinRace : SendMsg
{
    public BotId botId;
    public string trackName;
    public string password;
    public int carCount;

    public JoinRace(string name, string key, string trackName, string password, int carCount)
    {
        this.botId = new BotId(name, key);
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    protected override string MsgType()
    {
        return "joinRace";
    }

    protected override int MsgTick()
    {
        return 0;
    }
}

class Throttle : SendMsg
{
    public double value;
    public int gameTick;

    public Throttle(double value, int gameTick)
    {
        this.value = value;
        this.gameTick = gameTick;
    }

    protected override Object MsgData()
    {
        return this.value;
    }

    protected override string MsgType()
    {
        return "throttle";
    }

    protected override int MsgTick()
    {
        return gameTick;
    }
}

class SwitchLane : SendMsg
{
    public string value;
    public int gameTick;

    public SwitchLane(string value, int gameTick)
    {
        this.value = value;
        this.gameTick = gameTick;
    }

    protected override Object MsgData()
    {
        return this.value;
    }

    protected override string MsgType()
    {
        return "switchLane";
    }

    protected override int MsgTick()
    {
        return gameTick;
    }
}

class LanePosition
{
    public int startLaneIndex;
    public int endLaneIndex;
}

class PiecePosition
{
    public int pieceIndex;
    public double inPieceDistance;
    public LanePosition lane;
    public int lap;

    public PiecePosition(int pieceIndex, double inPieceDistance, LanePosition lane, int lap)
    {
        this.pieceIndex = pieceIndex;
        this.inPieceDistance = inPieceDistance;
        this.lane = lane;
        this.lap = lap;
    }
}

class CarPosition
{
    public CarId id;
    public double angle;
    public PiecePosition piecePosition;
    public int prevCommandTick;
}

class CarId
{
    public string name;
    public string color;
}

class CarPositions
{
    public string msgType;
    public CarPosition[] data;
    public string gameId;
    public int gameTick;

    public CarPositions()
    {
        gameTick = -1;
    }
}

class Piece
{
    public double length;
    public int radius;
    public double angle;
    public bool @switch;

    public override string ToString()
    {
        return "length: " + length + ", radius: " + radius + ", angle: " + angle + ", switch: " + @switch;
    }

    public double getRadius(double startLaneOffset, double endLaneOffset, double piecePosition)
    {
        // TODO: This doesn't actually work correctly with lane switching, but
        //       should be better than nothing.
            double phase = piecePosition / getLength(startLaneOffset, endLaneOffset);
            startLaneOffset += Math.Sign(angle) * Math.Abs(startLaneOffset - endLaneOffset) / 2;
        return getRadiusWithPhase(startLaneOffset, endLaneOffset, phase);
    }

    public double getRadiusWithPhase(double startLaneOffset, double endLaneOffset, double phase)
    {
        return radius - ((1.0 - phase) * startLaneOffset + phase * endLaneOffset) * Math.Sign(angle);
    }
	
	public double getLength(double startLaneOffset, double endLaneOffset)
    {
        if (radius <= 0.0)
        {
			double multiplier = (startLaneOffset != endLaneOffset ? 1.001 : 1.0);
            return Math.Sqrt(Math.Pow(length, 2.0) + Math.Pow(startLaneOffset - endLaneOffset, 2.0)) * multiplier;
        }
        else
        {
			            // TODO: This doesn't actually work correctly, but should be close enough
			double phase = 0.5;
			if (Math.Sign(endLaneOffset - startLaneOffset) == Math.Sign(angle))
			  phase = 0.4990;
			else
			  phase = 0.501;
            return Math.Sqrt(Math.Pow(getRadiusWithPhase(startLaneOffset, endLaneOffset, phase) * Math.Abs(angle) * Math.PI / 180.0, 2.0) + Math.Pow(startLaneOffset - endLaneOffset, 2.0));
        }
    }
	
    /*public double getLength(double startLaneOffset, double endLaneOffset)
    {
        if (radius <= 0.0)
        {
            return Math.Sqrt(Math.Pow(length, 2.0) + Math.Pow(startLaneOffset - endLaneOffset, 2.0));
        }
        else
        {
            // TODO: This doesn't actually work correctly, but should be close enough
            return Math.Sqrt(Math.Pow(getRadiusWithPhase(startLaneOffset, endLaneOffset, 0.5) * Math.Abs(angle) * Math.PI / 180.0, 2.0) + Math.Pow(startLaneOffset - endLaneOffset, 2.0));
        }
    }*/
}

class Lane
{
    public int distanceFromCenter;
    public int index;

    public override string ToString()
    {
        return "distanceFromCenter: " + distanceFromCenter + ", index: " + index;
    }
}

class Track
{
    public string id;
    public string name;
    public Piece[] pieces;
    public Lane[] lanes;
    public Object startingPoint;
}

class RaceSession
{
    public int laps;
    public int maxLapTimeMs;
    public bool quickRace;
}

class RaceInfo
{
    public Track track;
    public CarInfo[] cars;
    public RaceSession raceSession;
}

class CarInfo
{
    public CarId id;
    public CarDimensions dimensions;
}

class CarDimensions
{
    public double length;
    public double width;
    public double guideFlagPosition;
}

class Race
{
    public RaceInfo race;
}

class GameInit
{
    public string msgType;
    public Race data;
}

class Crash
{
    public string msgType;
    public CarId data;
    public string gameId;
    public int gameTick;
}

class Spawn
{
    public string msgType;
    public CarId data;
    public string gameId;
    public int gameTick;
}

class TurboStart
{
    public string msgType;
    public CarId data;
    public int gameTick;
}

class TurboEnd
{
    public string msgType;
    public CarId data;
    public int gameTick;
}

class YourCar
{
    public string msgType;
    public CarId data;
}

class Turbo : SendMsg
{
    public string value;
    public int gameTick;

    public Turbo(int gameTick)
    {
        this.value = "Jingle all the way!";
        this.gameTick = gameTick;
    }

    protected override Object MsgData()
    {
        return this.value;
    }

    protected override string MsgType()
    {
        return "turbo";
    }

    protected override int MsgTick()
    {
        return gameTick;
    }
}

class TurboAvailable
{
    public string msgType;
    public TurboData data;
}

class TurboData
{
    public double turboDurationMilliseconds;
    public int turboDurationTicks;
    public double turboFactor;
}

public class RaceLog
{
    public RaceLogEntry[] data;

    public RaceLog(RaceLogEntry[] data)
    {
        this.data = data;
    }
}

public class RaceLogEntry
{
    public double carVelocity;
    public double carAngle;
    public double curveRadius;

    public RaceLogEntry(double carVelocity, double carAngle, double curveRadius)
    {
        this.carVelocity = carVelocity;
        this.carAngle = carAngle;
        this.curveRadius = curveRadius;
    }
}
