Victim (the one at front) velocity (+/- 0.001) after collision:

  (attacker_velocity * 0.9 * friction) + throttle * engine_power


Attacker velocity (+/- 0.05) after collision:

  (victim_velocity * 0.784 * friction) + throttle * engine_power