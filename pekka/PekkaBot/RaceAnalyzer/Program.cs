﻿using System;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class RaceAnalyzer
{
    public static void Main(string[] args)
    {
        string line = System.IO.File.ReadAllText ("raceLog.json");

        RaceLog log = JsonConvert.DeserializeObject<RaceLog> (line);

        new RaceAnalyzer (log);
    }

    public RaceAnalyzer (RaceLog log)
    {
        Random random = new Random ((int)DateTime.Now.Ticks);
        AnglePredictionCoefficients[] coeffPool = new AnglePredictionCoefficients[100];
        double[] errors = new double[100];
        int[] bestIndices = new int[10];

//        for (int i = 0; i < coeffPool.Length; ++i) {
//            coeffPool [i] = new AnglePredictionCoefficients (random.NextDouble (), random.NextDouble (), random.NextDouble ());
//            errors [i] = 0.0;
//        }

        double leastError = 100.0;

//        for (int i = 0; i < 100000; ++i) {
//            for (int j = 0; j < errors.Length; ++j) {
//                errors [j] = 0.0;
//            }

        for (double angleCoeff = -0.1; angleCoeff <= 0.1; angleCoeff += 0.0001) {
            for (double centripetal = -0.1; centripetal <= 0.1; centripetal += 0.001) {
//                for (double throttleCoeff = -0.0026; throttleCoeff <= -0.002; throttleCoeff += 0.01) {
                for (double dAngleCoeff = -0.2; dAngleCoeff <= 0.2; dAngleCoeff += 0.001) {
//                        for (double offset = -1.0; offset <= 1.0; offset += 0.01) {
                            double largestError = 0.0;
//                    for (int k = 0; k < coeffPool.Length; ++k) {
                            double angle = log.data [0].carAngle;
                            double dAngle = angle;
                            for (int j = 1; j < log.data.Length - 1; ++j) {
                                double error = Math.Abs (angle - log.data [j].carAngle);
                                if (error > largestError) {
                                    largestError = error;
                                }
                                if (error > leastError) {//leastError) {
                                    break;
//                            errors[k] = error;
                                }

                        double velocity = log.data [j].carVelocity;
                        double radius = log.data [j].curveRadius;

                        double ddAngle = centripetal * (Math.Abs (radius) > 0.0 ? Math.Max (velocity - Math.Sqrt(Math.Abs (radius) * 0.32), 0.0) * Math.Sign (radius) : 0.0) -
                                dAngleCoeff * dAngle -
                            angleCoeff * angle*velocity;
    //                                    offset;//* angle * Math.Sign(angle);

                                dAngle += ddAngle;
                                angle += dAngle;
                            }

                            if (leastError > largestError) {
                        Console.WriteLine ("Centripetal: " + centripetal + ", angle: " + angleCoeff + ", dAngle: " + dAngleCoeff +/* ", throttle: " + throttleCoeff +*/ /*", offset: " + offset +*/ " - error: " + (largestError * 180 / Math.PI) + " degrees");
                                leastError = largestError;
                            }
//                        }
                    }
//                }
            }
        }
//                    }

//            for (int j = 0; j < bestIndices.Length; ++j) {
//                bestIndices [j] = -1;
//            }
//
//            for (int k = 0; k < coeffPool.Length; ++k) {
//                int worstIndex = -1;
//
//                for (int j = 0; j < bestIndices.Length; ++j) {
//                    if (bestIndices [j] == -1) {
//                        bestIndices [j] = k;
//                        break;
//                    }
//                    if (errors [k] < errors [bestIndices [j]]) {
//                        if (worstIndex == -1 || errors [bestIndices [j]] > errors [bestIndices [worstIndex]]) {
//                            worstIndex = j;
//                        }
//                    }
//                }
//
//                if (worstIndex != -1) {
//                    bestIndices [worstIndex] = k;
//                }
//            }
//
//            AnglePredictionCoefficients[] bestCoeffs = new AnglePredictionCoefficients[bestIndices.Length+1];
//            if (i == 100000 - 1 || i % 100 == 0) {
//                Console.WriteLine ("Batch #" + i);
//            }
//            for (int j = 0; j < bestIndices.Length; ++j) {
//                bestCoeffs [j] = coeffPool [bestIndices [j]];
//                if (i == 100000 - 1 || i % 1000 == 0) {
//                    Console.Write(bestCoeffs [j]);
//                    Console.WriteLine (" - error: " + errors [bestIndices [j]]);// + ", average: " + (errors[bestIndices[j]]/log.data.Length));
//                }
//            }
////            Console.WriteLine ("");
//
//            bestCoeffs [bestCoeffs.Length - 1] = new AnglePredictionCoefficients (random.NextDouble (), random.NextDouble (), random.NextDouble ());
//
//            for (int j = 0; j < bestCoeffs.Length; ++j) {
//                coeffPool[j] = bestCoeffs [j];
//            }
//            for (int j = bestCoeffs.Length; j < coeffPool.Length; ++j) {
//                AnglePredictionCoefficients father = bestCoeffs [random.Next () % bestCoeffs.Length];
//                AnglePredictionCoefficients mother = bestCoeffs [random.Next () % bestCoeffs.Length];
//
//                AnglePredictionCoefficients child;
//                child = new AnglePredictionCoefficients ((father.centripetal + mother.centripetal) / 2 + random.NextDouble () * 1.0 - 0.5,
//                    (father.angle + mother.angle) / 2 + random.NextDouble () * 1.0 - 0.5,
//                    (father.dAngle + mother.dAngle) / 2 + random.NextDouble () * 1.0 - 0.5);
//                coeffPool [j] = child;
//            }
//        }
    }
}

class AnglePredictionCoefficients
{
    public double centripetal;
    public double angle;
    public double dAngle;

    public AnglePredictionCoefficients(double centripetal, double angle, double dAngle)
    {
        this.centripetal = centripetal;
        this.angle = angle;
        this.dAngle = dAngle;
    }

    override public string ToString() {
        return "centripetal: " + centripetal + ", angle: " + angle + " dAngle: " + dAngle;
    }
}
