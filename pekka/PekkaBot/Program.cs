﻿using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

public class Bot
{
    public static void Main(string[] args)
    {
        string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];

        Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        using (TcpClient client = new TcpClient(host, port))
        {
            NetworkStream stream = client.GetStream();
            StreamReader reader = new StreamReader(stream);
            StreamWriter writer = new StreamWriter(stream);
            writer.AutoFlush = true;

            new Bot (reader, writer, new JoinRace (botName, botKey, "usa", "akasdkdsamkp", 1));
        }
    }

    private StreamWriter writer;

    Bot(StreamReader reader, StreamWriter writer, SendMsg join)
    {
        this.writer = writer;
        string line;

        send(join);

        List<RaceLogEntry> raceLog = new List<RaceLogEntry>();

        int numLaps = 0;

        double lastPosition = 0.0;
        double lastVelocity = 0.0;
        double lastAngle = 0.0;
		double largestAngle = 0.0;
        double lastThrottle = 0.0;
		Piece lastPiece = null;

        TurboData turboData = null;
        double currentTurboFactor = 1.0;
        int turboTicksLeft = 0;
        bool fullOnTurbo = false;

		double friction = 0.98;
        double enginePower = 0.2;

        //double safeSpeed = 4.0;//Math.Sqrt (0.32 * 90); //sqrt(28.8), 28.8 = 0.32 * 90 (90 = curve radius)

        string myCarColor = "";

        Piece[] pieces = new Piece[0];
		Lane[] lanes = new Lane[0];
        int[] laneSwitches = new int[0];
		int pieceId = 0;
        int switchLanes = 0;

        double targetVelocity = 4.0;

        while ((line = reader.ReadLine()) != null)
        {
            MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
            switch (msg.msgType)
            {
            case "gameInit":
                GameInit init = JsonConvert.DeserializeObject<GameInit> (line);

                numLaps = init.data.race.raceSession.laps;
                pieces = init.data.race.track.pieces;
                lanes = init.data.race.track.lanes;
                laneSwitches = new int[pieces.Length];
                int currentSwitchPiece = -1;

                double laneValue = 0.0;
                int pieceCount = pieces.Length;
                for (int i = 0; i < pieceCount; ++i) {
                    Piece piece = pieces [(i + 1) % pieces.Length];

                    if (piece.radius > 0) {
                        laneValue += piece.angle / piece.radius;
                    }

                    if (piece.@switch) {
                        if (currentSwitchPiece >= 0) {
                            laneSwitches [currentSwitchPiece] = (laneValue > 0.0 ? 1 : (laneValue < 0.0 ? -1 : 0));
                        } else {
                            pieceCount += i + 1;
                        }
                        laneValue = 0.0;
                        currentSwitchPiece = i;
                    } else {
                        laneSwitches [i % pieces.Length] = 0;
                    }
                }

                Console.WriteLine("Race init");
                Console.WriteLine(line);
//                send(new Ping());
                break;
            case "yourCar":
                Console.WriteLine (line);
				YourCar yourCar = JsonConvert.DeserializeObject<YourCar> (line);

				myCarColor = yourCar.data.color;
				break;
            case "carPositions":
                CarPositions carPositions = JsonConvert.DeserializeObject<CarPositions> (line);

                CarPosition myCar = null;

                foreach (CarPosition carPos in carPositions.data) {
                    if (carPos.id.color == myCarColor) {
                        myCar = carPos;
                    }
                }

                pieceId = myCar.piecePosition.pieceIndex;
                double pieceDistance = myCar.piecePosition.inPieceDistance;

                double currentAngle = myCar.angle;
                double angleDiff = currentAngle - lastAngle;
                int currentLane = myCar.piecePosition.lane.endLaneIndex;
                double laneOffset = lanes [currentLane].distanceFromCenter;

                double position = myCar.piecePosition.inPieceDistance;
                double velocity = position - lastPosition;

                if (lastPiece != null && lastPiece != pieces [pieceId]) {
//                    Console.WriteLine ("Hopped over a piece edge");
                    double currentVelocity = (lastVelocity * friction + lastThrottle * enginePower);
//                    Console.WriteLine ("Last position: " + lastPosition);
//                    Console.WriteLine ("Position: " + position); 
//                    Console.WriteLine ("Current velocity: " + currentVelocity);
//                    Console.WriteLine ("Last piece: " + ((pieceId - 1) % pieces.Length));

                    velocity = currentVelocity;//position + (lastPiece.getLength (laneOffset) - lastPosition);
//                    Console.WriteLine ("Velocity based on pos: " + velocity);

                    switchLanes = laneSwitches [pieceId];

                    if (pieceId == 0) {
                        fullOnTurbo = false;
                    }

                    if (lastPiece.radius > 0.0 && pieces [pieceId].radius == 0) {
                        targetVelocity += 1.0;
                        Console.WriteLine ("Increased velocity to : " + targetVelocity);
                    }
                }
                double acceleration = velocity - lastVelocity;

                if (lastVelocity == 0.0 && lastThrottle > 0.0) {
                    double oldEnginePower = enginePower;
                    enginePower = velocity / lastThrottle;
                    if (Math.Abs (enginePower - oldEnginePower) > 0.001) {
                        Console.WriteLine ("Dynamically calculated engine power to be: " + enginePower);
                    }
                }
                if (lastVelocity > 0.0) {
                    double oldFriction = friction;
                    friction = (velocity - lastThrottle * enginePower * currentTurboFactor) / lastVelocity;
                    if (Math.Abs (friction - oldFriction) > 0.00001) {
                        Console.WriteLine ("Dynamically calculated friction to be: " + friction);
                    }
                }

                if (Math.Abs (currentAngle) > Math.Abs (largestAngle)) {
                    largestAngle = currentAngle;
                    //Console.WriteLine ("Largest angle: " + largestAngle + " on piece " + pieceId);
                }

                double throttle = 1.0;
                double futureVelocity = velocity * friction + enginePower * currentTurboFactor;
                double futureAngle = currentAngle * Math.PI / 180.0;
                double futureDAngle = angleDiff * Math.PI / 180.0;
                int futurePieceId = pieceId;
                double futureRadius = pieces [futurePieceId].getRadius (laneOffset) * Math.Sign (pieces [futurePieceId].angle);
                double futurePieceDistance = pieceDistance;
                int futureLaneId = currentLane;
                double futureLaneOffset = laneOffset;
                double A = 0.075;
                double B = -0.11571;
                double C = -0.007671;
                for (int i = 0; i < 50; ++i) {

                    double nextDDAngle = A * (Math.Abs (futureRadius) > 0.0 ? Math.Max (futureVelocity * futureVelocity / Math.Abs (futureRadius) - 0.32, 0.0) * Math.Sign (futureRadius) : 0.0) +
                                         B * futureDAngle +
                                         C * futureAngle;
                    futureDAngle += nextDDAngle;
                    futureAngle += futureDAngle;

                    if (Math.Abs (futureAngle) >= 55 * Math.PI / 180.0) { //3.0) {
                        if (i <= 1) {
                            Console.WriteLine ("Predicting crash in " + i + " + ticks, with angle going up to " + (futureAngle * 180 / Math.PI));
                        }
                        throttle = 0.0;
                        break;
                    }

                    futureVelocity = futureVelocity * friction; // Assuming 0.0 throttle here.
                    futurePieceDistance += futureVelocity;
                    while (futurePieceDistance >= pieces [futurePieceId].getLength (futureLaneOffset)) {
                        futurePieceDistance -= pieces [futurePieceId].getLength (futureLaneOffset);
                        futurePieceId = (futurePieceId + 1) % pieces.Length;
                        futureLaneId = futureLaneId + laneSwitches [(futurePieceId + laneSwitches.Length - 1) % laneSwitches.Length];
                        if (futureLaneId >= lanes.Length) {
                            futureLaneId = lanes.Length - 1;
                        } else if (futureLaneId < 0) {
                            futureLaneId = 0;
                        }
                        futureLaneOffset = lanes [futureLaneId].distanceFromCenter;
                        futureRadius = pieces [futurePieceId].getRadius (futureLaneOffset) * Math.Sign (pieces [futurePieceId].angle);
                    }
                }
//                if (velocity > 0) {
//                    Console.WriteLine ("Throttle: " + throttle);
//                }
                if (turboTicksLeft > 0) {
                    --turboTicksLeft;
                } else {
                    currentTurboFactor = 1.0;
                }
                bool turboUsed = false;
                if (throttle == lastThrottle && turboData != null) {// && myCar.piecePosition.lap == numLaps - 1) {
                    futureVelocity = velocity * friction + enginePower * turboData.turboFactor * lastThrottle;
                    futureAngle = currentAngle * Math.PI / 180.0;
                    futureDAngle = angleDiff * Math.PI / 180.0;
                    futurePieceId = pieceId;
                    futureRadius = pieces [futurePieceId].getRadius (laneOffset) * Math.Sign (pieces [futurePieceId].angle);
                    futurePieceDistance = pieceDistance;
                    futureLaneId = currentLane;
                    futureLaneOffset = laneOffset;
                    bool safe = true;
                    bool breakAll = false;
                    for (int i = 0; i < turboData.turboDurationTicks * 2; ++i) {

                        double nextDDAngle = A * (Math.Abs (futureRadius) > 0.0 ? Math.Max (futureVelocity * futureVelocity / Math.Abs (futureRadius) - 0.32, 0.0) * Math.Sign (futureRadius) : 0.0) +
                                             B * futureDAngle +
                                             C * futureAngle;
                        futureDAngle += nextDDAngle;
                        futureAngle += futureDAngle;

                        if (Math.Abs (futureAngle) >= 59.9 * Math.PI / 180.0) {
                            safe = false;
                            break;
                        }

                        double turboFactor = (turboData.turboDurationTicks - i > 0 ? turboData.turboFactor : 1.0);
                        futureVelocity = futureVelocity * friction + enginePower * turboFactor; // Assuming 0.0 throttle here.
                        futurePieceDistance += futureVelocity;
                        while (futurePieceDistance >= pieces [futurePieceId].getLength (futureLaneOffset)) {
                            futurePieceDistance -= pieces [futurePieceId].getLength (futureLaneOffset);
                            futurePieceId = (futurePieceId + 1) % pieces.Length;
                            if (futurePieceId == 0) {
                                if (myCar.piecePosition.lap == numLaps - 1) {
                                    fullOnTurbo = true;
                                    Console.WriteLine ("Full on turbo");
                                }
                                safe = true;
                                breakAll = true;
                                break;
                            }
                            futureLaneId = futureLaneId + laneSwitches [(futurePieceId + laneSwitches.Length - 1) % laneSwitches.Length];
                            if (futureLaneId >= lanes.Length) {
                                futureLaneId = lanes.Length - 1;
                            } else if (futureLaneId < 0) {
                                futureLaneId = 0;
                            }
                            futureLaneOffset = lanes [futureLaneId].distanceFromCenter;
                            futureRadius = pieces [futurePieceId].getRadius (futureLaneOffset) * Math.Sign (pieces [futurePieceId].angle);
                        }
                        if (breakAll)
                            break;
                    }

                    if (safe) {
                        send (new Turbo ());
                        currentTurboFactor = turboData.turboFactor;
                        turboTicksLeft = turboData.turboDurationTicks;
                        turboData = null;
                        turboUsed = true;
                        Console.WriteLine ("Turbo");
                    }
                }

                throttle = Math.Min(Math.Max((targetVelocity - velocity * friction) / (enginePower * currentTurboFactor), 0.0), 1.0);

                if (carPositions.gameTick != -1) {
                    if (!turboUsed) {
                        if (throttle == lastThrottle && switchLanes != 0) {
                            send (new SwitchLane (switchLanes > 0 ? "Right" : "Left", carPositions.gameTick));
                        } else {
                            if (currentTurboFactor > 1.0 && fullOnTurbo) {
                                //                            Console.WriteLine ("TURBO");
                                send (new Throttle (1.0, carPositions.gameTick));
                            } else {
                                send (new Throttle (throttle, carPositions.gameTick));
                            }
                        }
                    }
                    lastThrottle = throttle;
                    lastVelocity = velocity;
                } else {
                    Console.WriteLine ("Gametick not given, so assuming start of a race");
                    lastThrottle = 0.0;
                    lastVelocity = 0.0;
                }

                lastPiece = pieces [pieceId];
                lastPosition = position;
                lastAngle = currentAngle;

                raceLog.Add (new RaceLogEntry (velocity, currentAngle * Math.PI / 180.0, pieces [pieceId].getRadius (laneOffset) * Math.Sign (pieces [pieceId].angle)));
                break;
            case "join":
                Console.WriteLine("Joined");
                Console.WriteLine (line);
                break;
            case "gameEnd":
                Console.WriteLine ("Race ended");
                Console.WriteLine ("Largest angle reached: " + largestAngle);
                Console.WriteLine (line);

                string jsonOutput = JsonConvert.SerializeObject (new RaceLog (raceLog.ToArray ()));

                System.IO.File.WriteAllText ("raceLog.json", jsonOutput);

                string csvOutput = "Car velocity, Curve radius, Car angle";
                foreach (RaceLogEntry entry in raceLog) {
                    csvOutput += "\n" + entry.carVelocity + ", " + entry.curveRadius + ", " + entry.carAngle;
                }
                System.IO.File.WriteAllText ("raceLog.csv", csvOutput);

                break;
            case "gameStart":
                Console.WriteLine("Race starts");
                Console.WriteLine (line);
                send(new Ping());
                break;
            case "crash":
                Crash crash = JsonConvert.DeserializeObject<Crash> (line);
                Console.WriteLine ("Crashed on tick " + crash.gameTick + " track piece " + pieceId);

                targetVelocity = 5.0;
				break;
            case "turboAvailable":
                TurboAvailable turbo = JsonConvert.DeserializeObject<TurboAvailable> (line);
                turboData = turbo.data;
                break;
            default:
                Console.WriteLine (line);
                break;
            }
        }
    }

    private void send(SendMsg msg)
    {
        writer.WriteLine(msg.ToJson());
    }
}

class MsgWrapper
{
    public string msgType;
    public Object data;

    protected MsgWrapper()
    {
    }

    public MsgWrapper(string msgType, Object data)
    {
        this.msgType = msgType;
        this.data = data;
    }
}

class ReceiveMsg : MsgWrapper
{
    public string gameId;
    public int gameTick;

    public ReceiveMsg(string msgType, Object data, string gameId, int gameTick)
    {
        this.msgType = msgType;
        this.data = data;
        this.gameId = gameId;
        this.gameTick = gameTick;
    }
}

abstract class SendMsg
{
    public string ToJson()
    {
        return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
    }
    protected virtual Object MsgData()
    {
        return this;
    }

    protected abstract string MsgType();
}

class Join : SendMsg
{
    public string name;
    public string key;

    public Join(string name, string key)
    {
        this.name = name;
        this.key = key;
    }

    protected override string MsgType()
    {
        return "join";
    }
}

class BotId {
    public string name;
    public string key;

    public BotId(string name, string key)
    {
        this.name = name;
        this.key = key;
    }
}

class JoinRace : SendMsg
{
    public BotId botId;
    public string trackName;
    public string password;
    public int carCount;

    public JoinRace(string name, string key, string trackName, string password, int carCount)
    {
        this.botId = new BotId (name, key);
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    protected override string MsgType()
    {
        return "joinRace";
    }
}

class Ping : SendMsg
{
    protected override string MsgType()
    {
        return "ping";
    }
}

class Throttle : SendMsg
{
    public double value;
    public int gameTick;

    public Throttle(double value, int gameTick)
    {
        this.value = value;
        this.gameTick = gameTick;
    }

    protected override Object MsgData()
    {
        return this.value;
    }

    protected override string MsgType()
    {
        return "throttle";
    }
}

class SwitchLane : SendMsg
{
    public string value;
    public int gameTick;

    public SwitchLane(string value, int gameTick)
    {
        this.value = value;
        this.gameTick = gameTick;
    }

    protected override Object MsgData()
    {
        return this.value;
    }

    protected override string MsgType()
    {
        return "switchLane";
    }
}

class LanePosition
{
	public int startLaneIndex;
	public int endLaneIndex;
}

class PiecePosition
{
	public int pieceIndex;
	public double inPieceDistance;
	public LanePosition lane;
	public int lap;

	public PiecePosition(int pieceIndex, double inPieceDistance, LanePosition lane, int lap)
	{
		this.pieceIndex = pieceIndex;
		this.inPieceDistance = inPieceDistance;
		this.lane = lane;
		this.lap = lap;
	}
}

class CarPosition
{
	public CarId id;
	public double angle;
	public PiecePosition piecePosition;
}

class CarId
{
	public string name;
	public string color;
}

class CarPositions
{
	public string msgType;
	public CarPosition[] data;
	public string gameId;
	public int gameTick;

	public CarPositions()
    {
        gameTick = -1;
	}
}

class Piece
{
	public double length;
	public int radius;
	public double angle;
	public bool @switch;

	public override string ToString()
	{
		return "length: " + length + ", radius: " + radius + ", angle: " + angle + ", switch: " + @switch;
	}

	public double getRadius(double laneOffset)
	{
		return radius - laneOffset * Math.Sign (angle);
	}

	public double getSafeSpeed(double laneOffset)
	{
		if (radius <= 0.0)
		{
			return 10.0;
		}
		else
		{
            // F = mv^2 / r = m * (Math.Sqrt(0.32 * r))^2 / r = m * 0.32 * r / r = m * 0.32
			return Math.Sqrt(0.32 * getRadius(laneOffset));
		}
	}

	public double getLength(double laneOffset)
	{
		if (radius <= 0.0)
		{
			return length;
		}
		else
		{
			return getRadius(laneOffset) * Math.Abs(angle) * Math.PI / 180.0;
		}
	}
}

class Lane
{
	public int distanceFromCenter;
	public int index;

	public override string ToString()
	{
		return "distanceFromCenter: " + distanceFromCenter + ", index: " + index;
	}
}

class Track
{
	public string id;
	public string name;
	public Piece[] pieces;
	public Lane[] lanes;
	public Object startingPoint;
}

class RaceSession
{
    public int laps;
    public int maxLapTimeMs;
    public bool quickRace;
}

class RaceInfo
{
	public Track track;
	public Object[] cars;
    public RaceSession raceSession;

}

class Race
{
	public RaceInfo race;
}

class GameInit
{
	public string msgType;
	public Race data;
}

class Crash
{
	public string msgType;
	public Object data;
	public string gameId;
	public int gameTick;
}

class YourCar
{
	public string msgType;
	public CarId data;
}

class Turbo : SendMsg
{
    public string value;

    public Turbo()
    {
        this.value = "Jingle all the way!";
    }

    protected override Object MsgData()
    {
        return this.value;
    }

    protected override string MsgType()
    {
        return "turbo";
    }
}

class TurboAvailable
{
    public string msgType;
    public TurboData data;

}

class TurboData
{
    public double turboDurationMilliseconds;
    public int turboDurationTicks;
    public double turboFactor;
}