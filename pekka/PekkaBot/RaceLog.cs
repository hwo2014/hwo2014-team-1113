﻿using System;

public class RaceLog
{
    public RaceLogEntry[] data;

    public RaceLog(RaceLogEntry[] data) {
        this.data = data;
    }
}

public class RaceLogEntry
{
    public double carVelocity;
    public double carAngle;
    public double curveRadius;

    public RaceLogEntry(double carVelocity, double carAngle, double curveRadius) {
        this.carVelocity = carVelocity;
        this.carAngle = carAngle;
        this.curveRadius = curveRadius;
    }
}